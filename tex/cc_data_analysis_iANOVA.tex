\subsection{Analysis of Influence Factors}
For further analysis, the observations are grouped by different factors, (e.g. \textit{time of day, hospital, incident type}).
The groups are then compared by their mean and variance, or in this case their standard deviation. 
Standard deviation was chosen over variance as it is in the same order of magnitude and in the same unit of measurement as the values and means. 
Seltman \parencite*{seltman_exp_design_2018} argues that this comparison of means and variances constitutes a simplified and informal version of an analysis of variance [ANOVA].
This method was chosen because a regular ANOVA is parametric, meaning it requires among other things a normal distribution of values, 
which is not present in this dataset. 

When comparing multiple factors, which is to say one group under the condition of another (e.g. \textit{incident type} \textsc{ACS} 
at \textit{time of day} \textsc{6:00 - 11:59}), 
each group's mean and variance is shown in a matrix. Note that the respective counts per group and the continuous highest density intervals are 
presented in the appendix. 
This was done because some groups have a low count of observations, which should be taken into account when interpreting the data. 


\subsubsection{Time of Day}
When analysing the data, one of the first ideas was to look at the diurnal variation of turnaround times. 
First, a boxplot visualization was chosen (Figure \ref{fig:box24h}), which prooved less than ideal due to the large number of \textit{outliers}, meaning values
more than 1.5 times the interquartile range above the third quartile, which are typically shown as data points outside the whiskers. 

Plotting the mean and standard deviation (Figure \ref{fig:bar24h}), the lowest mean turnaround times are noted at night with a very distinct peak at 6:00 to 6:59. 
This may be related to shift changes occurring around the same time, which may also in part explain the high standard deviation.
This peak however is much less distinct in the evening, although ambulance crew shift changes also occur from 19:00 to 20:00.   

In an attempt to understand the increase of variance around 6:00, histograms for exemplary groups are shown in Figure \ref{fig:tod_hists} and their 
mean, median, variance, and other measures in Table \ref{tab:results_tod}

\begin{figure}[h!]
    \begin{center}
        \input{fig/analysis_iANOVA/box24hours.pgf}
    \end{center}
    \caption{Diurnal variation of turnaround times as boxplots, potentially misleading}
    \label{fig:box24h}
\end{figure}
\begin{figure}[h!]
    \centering
    \scalebox{0.8}{\input{fig/analysis_iANOVA/bar_HourOfDay.pgf}}
    \caption{Diurnal variation of mean turnaround times and their standard deviation }
    \label{fig:bar24h}
\end{figure}

\begin{figure*}[h!]
    \begin{subfigure}[t]{4cm}
        \centering
        \scalebox{0.48}{\input{fig/analysis_iANOVA/histogram_tod_6.pgf}}
        \caption{6:00 to 6:59}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{4cm}
        \centering
        \scalebox{0.48}{\input{fig/analysis_iANOVA/histogram_tod_11.pgf}}
        \caption{11:00 to 11:59}
    \end{subfigure}
    ~ 
    \begin{subfigure}[t]{4cm}
        \centering
        \scalebox{0.48}{\input{fig/analysis_iANOVA/histogram_tod_15.pgf}}
        \caption{15:00 to 15:59}
    \end{subfigure}
    \caption{Distribution of turnaround times by time of day, exemplary groups. See Table \ref{tab:results_tod} in the results chapter for details}
    \label{fig:tod_hists}

\end{figure*}

\subsubsection{Day of Week}
Mean turnaround time and its variance seems to change rather little throughout the week (Figure \ref{fig:dayofweek}). 

\begin{figure}[h!]
    \centering
    \scalebox{0.8}{\input{fig/analysis_iANOVA/bar_DayOfWeek.pgf}}
    \caption{Variation of turnaround times by day of week}
    \label{fig:dayofweek}
\end{figure}
\FloatBarrier
\subsubsection{Incident Type}
Plotting the ten most frequently observed \textit{incident types}, also refered to as \textit{codes}, major differences between the groups are noticeable (Figure \ref{fig:type}). 
Interestingly, \textsc{cardiac emergency} has one of the shortest mean turnaround times and for \textsc{stroke / brain hemorrage}, 
a long mean turnaround time with a low standard deviation is observed. 

Do note however that some \textit{codes} are closely related to each other and the dispatcher's choice of one \textit{code} over the other may be influenced by more than 
the factual information available when opening the call. 
Additional factors may be personal preference, familiarity, and level of training among other things. 


\begin{figure}[h!]
    \centering
    \scalebox{0.8}{\input{fig/analysis_iANOVA/bar_Code.pgf}}
    \caption{Variation of turnaround times by incident type}
    \label{fig:type}
\end{figure}

\subsubsection{Incident Type and Time of Day}
Aside from the general diurnal variation also found in Figure \ref{fig:bar24h}, the tabular representation of group-wise mean turnaround time
in Figure \ref{fig:code_and_tod_mean} has proven difficult to interpret. 
Figure \ref{fig:code_and_tod_std} however shows quite clearly that a small number of groups is responsible for a major part of the variance in the dataset. 

Keep in mind that some outliers in both Subfigure \ref{fig:code_and_tod_mean} and Subfigure \ref{fig:code_and_tod_std} may be explained by a low number of observations. 
Refer to Figure \ref{fig:code_and_tod_count} in the appendix for a group-wise count of observations. 

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[height=13cm]{fig/analysis_iANOVA/code_tod_mean.png}
        \caption{Mean turnaround time}
        \label{fig:code_and_tod_mean}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[height=13cm]{fig/analysis_iANOVA/code_tod_std.png}
        \caption{Standard Deviation}
        \label{fig:code_and_tod_std}
    \end{subfigure}
    \caption{Turnaround time by incident type and time of day}
    \label{fig:code_and_tod}
\end{figure*}

\subsubsection{Hospital}
Figure \ref{fig:hosp} shows noticeable differences between hospitals. These differences may for example be caused by usual workload, 
working culture, medical specialization or building layout. 

\begin{figure}[h!]
    \centering
    \scalebox{0.8}{\input{fig/analysis_iANOVA/bar_DestinationName.pgf}}
    \caption{Mean turnaround times by hospital and their respective variance}
    \label{fig:hosp}
\end{figure}

\subsubsection{Hospital and Time of Day}
Figure \ref{fig:hosp_and_tod} also proves difficult to interpret without controlling for the overall diurnal variation in mean and variance. 
Diverging turnaround times between the hospitals at a given \textit{time of day} may be caused by differences in staffing level, 
time of shift change or workload.  

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[height=13cm]{fig/analysis_iANOVA/hosp_tod_mean.png}
        \caption{Mean turnaround time}
        \label{fig:hosp_and_tod_mean}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[height=13cm]{fig/analysis_iANOVA/hosp_tod_std.png}
        \caption{Standard Deviation}
        \label{fig:hosp_and_tod_std}
    \end{subfigure}
    \caption{Turnaround time by hospital and time of day}
    \label{fig:hosp_and_tod}
\end{figure*}

\subsubsection{Hospital and Incident Type}
As shown in Figure \ref{fig:HospAndIncident}, some hospitals have noticeably higher turnaround times for particular \textit{codes}. 
This may for example be caused by medical specialization, leading to only more severe cases for a given \textit{code} 
may be transported to that hospital. 
Again, note that some outliers are caused by a low number of observations in the given category and refer to Figure \ref{fig:HospAndIncident_count} 
in the appendix for the count of observations per category. 

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[height=10cm]{fig/analysis_iANOVA/hosp_code_mean.png}
        \caption{Mean turnaround time}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[height=10cm]{fig/analysis_iANOVA/hosp_code_std.png}
        \caption{Standard Deviation}
    \end{subfigure}
    \caption{Turnaround time by hospital and incident code}
    \label{fig:HospAndIncident}
\end{figure*}