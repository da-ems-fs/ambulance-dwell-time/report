\section{Literature Review}
The field of emergency medical services is well studied and most issues are researched extensively by scientists in the field of medicine, operations research, 
and computer science. 
The present topic of ambulance turnaround time has become a focus in literature only in recent years, as Li et al. note \parencite{li_review_2019}. 
Whilst the focus in studies with a medical background tends to be the delivery interval, that is the time from arrival at the hospital to transfer of care, 
the turnaround time also includes all other processes after the transfer of care that are required for the ambulance to become available again. 

\subsection{Definitions}
\FloatBarrier
In international literature, turnaround time is defined as the time from an ambulance arriving at a hospital to it becoming available for dispatch again (see Figure \ref{fig:intervals}). It includes the 
delivery interval, the transfer of care to the hospital staff and the recovery interval in which ambulance crews get ready for the next call 
\parencite{spaite_prospective_1993,carter_can_2014, li_review_2019}. 

The generally agreed upon definition in Germany is the one used by the German Federal Highway Research Insitute [Bundesanstalt für Straßenwesen, BASt] 
in their reports on the performance of the rescue services (Figure \ref{fig:intervals_BASt}).
They define the \textbf{delivery interval} [Verweilzeit am Transportziel]
as the duration between the arrival at the destination [Ankunft am Transportziel] and the completion of the transfer of care [Abschluss der Patientenübergabe]. 
It includes the offloading of the patient, the transportation into the treatment facility [Verbringung zur behandelnden Einrichtung], 
the transfer of care [Übergabe an die behandelnde Einrichtung] as well as the time the crew requires to return to the ambulance [Rückkehr zum Rettungsmittel].
The \textbf{recovery interval} [Wiederherstellungszeit der Einsatzbereitschaft] is defined as the time between the return of the crew to the ambulance and the time to showing available again [Freimeldezeitpunkt] 
and includes all necessary cleaning and preparation [Rüst- und Reinigungsarbeiten]. They note that the recovery interval usually ends at the hospital, 
unless major disinfection and cleaning is required, which is generally performed at the ambulance station. 
In their reporting, they combine delivery and recovery interval [Verweilzeit am Transportziel/Wiederherstellungszeit], but do not name this combined interval. 
In the context of this research, it will be referred to as the \textbf{turnaround interval} or \textbf{turnaround time}. \parencite{schmiedel_leistungen_2019}

\begin{figure}
    \centering
    \includegraphics[width=11cm]{fig/intro/definition.pdf}
    \caption{Definition of time intervals \parencite{li_review_2019}}
    \label{fig:intervals}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=13cm]{fig/intro/definition_bast.pdf}
    \caption{Definition of time intervals as used by BASt \parencite{schmiedel_leistungen_2019}. Author's translation}
    \label{fig:intervals_BASt}
\end{figure}

\subsection{Studies on Turnaround Time and Ambulance Offload Delay}
In their review of 137 articles dealing with the topic of ambulance offload delay, Li et al.\parencite*{li_review_2019} note that ambulance offload delay 
appears to be a significant issue in the ambulance services of the USA, the UK, Canada and Australia, with a major cause appearing to be emergency department crowding. 
Eckstein et al.\parencite*{eckstein_facilitating_2005} add that this delay may not only negatively impact the patient being transferred but also have adverse effect on ambulance availability, 
thus increasing response times across the entire ambulance service. 
Another factor that Li et al.\parencite*{li_review_2019} note to be overlooked in previous literature is the effect on crew workload and occupational health. 

Comparing delivery and recovery interval, Carter et al.\parencite*{carter_can_2014} found that in a study of 1732 ambulance runs in Richmond, Virgina, USA in 2008 
that delivery time made up around 70\% of total turnaround time, although the time of transfer of care was recorded manually by the crews while timestamps 
for arrival and availability were logged automatically, which may have led to inaccuracies. 

\subsection{Causes and Predictors of Ambulance Offload Delay}
A common theme in literature is that the root cause of ambulance offload delay is hospital crowding \parencite{li_review_2019}, although not necessarily 
crowding just in the emergency department, but also in other hospital departments, leading to the patients not being able to leave the emergency department
because the receiving ward is at capacity \parencite{cooney_ambulance_2011,li_review_2019}. 

Cone et al. found that the most significant predictors of ambulance offload delay were size and location of the hospital, season, chief medical complaint
and patient age. 
Analyzing data from over 140 000 ambulance calls in New South Wales, Australia, they report that the most significant delays
were experienced during winter as well as in large hospitals in urban areas. Patients under the age of 16 and patients with cardiac complaints or major trauma
had the lowest likelhood of delays. \parencite{cone_analysis_2012}
\FloatBarrier
\subsection{Reported Turnaround Times and Delivery Intervals} \label{subs:lit_comp}
\FloatBarrier
This section lists literature values for turnaround time as found by other studies. 
A tabular overview of values published in international studies can be found in Table \ref{tab:lit_comp} in the Appendix.

The German Federal Highway Research Institute regularly publishes a report on the performance of the emergency services [Leistungen des Rettungsdienstes]. 
In their most recent report they note a mean turnaround time [Verweilzeit am Transportziel / Wiederherstellungszeit] of 19.3 minutes for incidents classified as an emergency but without an emergency doctor on scene 
and 19.7 minutes for emergency incidents with an emergency doctor on scene. \parencite{schmiedel_leistungen_2019} [$n$=\num{1 273 482}, computer aided dispatch data]

An overview of the mean turnaround times found in the previous reports can be found in Figure \ref{fig:BASt}.

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/bast/by_incident_type.png}
        \caption{by incident type}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/bast/by_call_category.png}
        \caption{by call type}
    \end{subfigure}
    \caption{Mean turnaround times as reported by BASt \parencite{schmiedel_leistungen_2019, schmiedel_leistungen_2007, schmiedel_leistungen_2011, schmiedel_leistungen_2015}. Author's translation}
    \label{fig:BASt}
\end{figure*}

\FloatBarrier