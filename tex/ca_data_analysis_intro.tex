\section{Data Analysis}
In the interest of reproducibility and transparency, the origin of the dataset as well as all steps taken to analyze it and to produce
the results shown below are documented extensively. 
The statistical methods used here are fairly simple and the author's rationale behind the choice of each analysis is explained. 
In the discussion chapter, these are reviewed critically and suggestions for further analyses are made. 

As is the nature of an exploratory data analysis, the analysis is not only guided by the research questions but also by data availability and 
the findings made during analysis. 
In this method, methodology and results are closely linked and therefore will be presented together here. 
A synopsis of the findings is given in the results chapter, answering each research question directly.  

\subsection{Toolchain and Technical Notes}
\label{subs:tech}
\href{https://www.python.org}{Python 3} is the main tool used in this project, utilizing the \href{https://pandas.pydata.org/}{pandas library} 
for data handling and \href{https://numpy.org/}{numpy} for mathematical functions.

The git repositories containing the source code of the analysis as well as the tex files of this document 
are made publicly available at \url{https://gitlab.com/da-ems-fs/ambulance-dwell-time}. 
Readers are encouraged to use this python code to analyze similar datasets that they have access to and to publish their results. 
Along with the code, the analysis repository also contains instructions for installation in the \textit{installation.md} file and lists the exact 
software versions used in the \textit{requirements.txt} file. 
Unfortunately the original dataset can not be made available for legal reasons, but the repository does contain an example dataset with some synthetic data.  

\subsection{Data Origin}
This section details how, where, and when the data was gathered and lists the steps taken to extract, transform and load the data in a usable format. 
Furthermore, data validation and sanity checks are described, followed by outlier removal and further preparation. 

\subsubsection{Source of Dataset}

Traditionally, German emergency services have used and to some extent are still using analog radio for voice communications. 
In the 1970s, a short data service was introduced that allows digital transmission of short messages via acoustic signals on analog voice channels using 
existing analog radios [Funkmeldesystem, FMS]. 
Its operating principle is similar to that of dual-tone multi-frequency signaling [DTMF] used in telephony for tone dialing, but technical implementation is rather different 
as frequency shift keying [FSK] is used. 
In the emergency services this system was widely adopted as it allows crews to report their current status to the command center by entering a one digit
numeric code into their radio's keypad. These codes are specified in the technical guidelines for emergency services and authorities 
[Technische Richtlinie der Behörden und Organisationen mit Sicherheitsaufgaben, TR-BOS], subsection Radio Message System [Funkmeldesystem, FMS]. 
Even after the introduction of digital radio communications, this numeric code is still in use and, with some exceptions, standardized throughout Germany. 

The dataset used here however was not generated using the incident control log of FMS status received via analog or digital radio.
Instead, it was exported from the rescuetrack database. Rescuetrack is a cloud-based dispatch, navigation and fleet management system that uses mobile 
terminals that are, among other things, wired into the emergency vehicle's radio. Therefore, whenever crews press a button on their radio to change the FMS status, 
the rescuetrack terminal in the vehicle logs this event along with a timestamp and the vehicle's position, both aquired via GPS. 
This event is then transmitted to rescuetrack servers via one of two SIM cards along with the original timestamp, meaning in contrast to FMS even a temporary loss of data connectivity 
does not change the event's timestamp.\parencite{tr_bos, cimolino_2008}

\subsubsection{Description of Study Area}
Esslingen is, by population, one of the largest administrative districts in the German federal state of Baden-Württemberg. 
In 2019 it had a total population of \num{535 000} with an area of just over \SI{640}{\square\kilo\metre}.
This equates to a population density of \SI[per-mode=repeated-symbol]{834}{\person\per\square\kilo\metre}, significantly above the German national average of \SI[per-mode=repeated-symbol]{232}{\person\per\square\kilo\metre}. 
The mean population age is 44 years, which is just below the national average. 
\parencite{destatis, stat-bw, stat-bw-age, bib}

\subsubsection{Overview of Dataset}
The dataset contains \num{43425} incidents attended by ambulances from January 1 to June 30, 2020. 
It was extracted from the rescuetrack database with the generous permission of  \textit{Integrierte Leistelle Esslingen} using a custom exporter provided by Convexis GmbH.
The custom exporter attempts to extract only those incidents that resulted in a patient transport to a hospital. 
Since this process relies on checking the DestinationName strings for components like "Hopspital" or "Hosp." or rather their German equivalents, it is likely somewhat flawed.  
The export was presented to the author as a tab-separated .CSV file. 
The dataset is well labeled and has self-explanatory column names like \textit{Month, DayOfWeek, HourOfDay} and \textit{DestinationName}.
Note that while data on the day of the week and the month is available, the day of the month has been redacted for patient privacy. 
This limits the suitability of this dataset for time series analysis. 
Columns containing time durations, such as \textit{SecondsAtDestination}, were converted to minutes by the author. 
After dropping rows containing Null values, 28 487 incidents remain. 
The \textit{Urgent} column was filtered for urgent emergency incidents, represented as the integer value 1, after which 12 813 incidents remained.
The \textit{Code} column contains a somewhat standardized incident code, the ten most frequent of which were translated to english by author (Table \ref{tab:code_translation}). 
The letter (N) or (D), respectively, signals that for this incident type, an emergency doctor [Notarzt] is also dispatched. 
The hospital names in the \textit{DestinationName} column are pseudonymized, i.e. replaced with hosp-N.
In the following, most categorical plots will only show the ten most frequent values for reasons of clarity. 
Even though the remaining values are not shown in these plots, they were not filtered out and are contained in other plots.  

\begin{table}
\centering
\begin{tabular}{ll}
    \textit{original} \texttt{code} & \textit{translation}    \\[0.2cm]
    \hline
    1019N - ACS/Infarkt (N)&ACS / heart attack (D)\\
    1020N - Bewusstlose Person (N)&unconscious person (D)\\
    1024N - Atemnot akut (N)&acute respiratory distress (D)\\
    1011 - Kollaps/Synkope&collapse / syncope\\
    1044 - Gestürzte Person&fall\\
    1044 - Gestürzte Person (N)&fall (D)\\
    1017N - Kardiologischer Notfall (N)&cardiac emergency\\
    1036 - Schlaganfall/Hirnblutung&stroke / brain hemorrhage\\
    1035N - Krampfanfall (N)&seizure (D)\\
    1099N - NA zur Schmerzbekämpfung (N)&doctor for pain management (D)\\
    1090N - Verlegung RTW und NEF (N)&transfer (D)\\    
\end{tabular}
\caption{Manual translation of the 10 most frequently observed incident codes}
\label{tab:code_translation}
\end{table}

\subsubsection{Outlier Detection and Removal}
Since the status changes are a manual process relying on crews pushing a button on their radio when becoming available again, the dataset contains some unquestionably incorrect values 
such as the maximum value at 887 hours. Aside from forgetting to push the button, transmission issues or callsign changes could potentially lead to incorrect values. 
Determining which values are incorrect and which represent genuine offload delays is difficult when only utilizing this dataset. 
After reviewing the literature and discussing the issue with a supervisor from \textit{Integrierte Leistelle Esslingen}, it was determined that turnaround times over 3 hours
most probably are incorrect. In a similar manner, crews may forget to signal their arrival at the hospital and realize this after the turnaround. 
They would then signal "arrival at hospital" and "unit available" in close succession. Therefore, turnaround times under 10 seconds are also included.  
After filtering out values over three hours, under 10 seconds, rows containing NaN values and non-urgent calls, \num{10569} rows remained.
Table \ref{tab:cleanup} gives a detailed overview of how these cleanup and outlier removal operations impacted the dataset length as well as mean turnaround time and its 90th percentile.  
While the chosen method of outlier removal, a best guess or expert rule, is controversial, the author feels that reasonable assumptions were made.
Other outlier removal techniques, for example simple ones based on the interquartile range, only improved scientific validity marginally while being more complicated 
to communicate and simultaniously sensitive to outliers themselves. 


\begin{table}
\centering
\begin{tabular}{llrrr}
    \textbf{\textit{order}} & \textit{removal operation} &  \textit{rows remaining} & \textit{mean [min]} &  \textit{p90 [min]}\\[0.2cm]
    \hline
    &&&&\\[-0.4cm]
   0. & raw &\num{43425} &72.6 &85.08 \\
   1. & NaN values &\num{28583} &74.86 &85.53 \\
   2. & urgent!=1 &\num{12813} &103.24 &186.95 \\
   3. & over 3 hours &\num{10751} &30.53 &57.52 \\
   4. & under 10 seconds &\num{10569} &30.72 &57.94 \\
\end{tabular}

\caption{Properties of the raw dataset and after each cleanup operation}
\label{tab:cleanup}
\end{table}


\subsubsection{Data Validation and Sanity Checks}
When comparing the mean, median and percentiles of this dataset with literature values, they seem plausible. 
The mean turnaround time is slightly higher than those reported by BASt. See also the literature review in chapter \ref{subs:lit_comp} and the results of this analysis in chapter \ref{subs:results}.

\subsection{Impact of the COVID-19 Pandemic}

During the COVID-19 pandemic, the number of calls attended by the ambulance service has declined. 
While the reasons for this decline are beyond the scope of this research, it can be speculated that lower traffic volumes led to a decrease in road accidents and that
those who perceived their conditions to be less serious may have been motivated not to call an ambulance due to fear of infection at the hospital. 
At the same time, turnaround times have increased, probably due to more thorough desinfections being necessary. 

As the pandemic gained public attention throughout Germany in March 2020, the call frequency decreased significantly and increases again as the number of new 
COVID-19 cases stabilizes at a lower level. The increase in mean turnaround times begins in Febuary with an increase of 10\% relative to January and remains 5 to 10\% 
higher than January throughout the observed period. 
As the number of new cases reaches its highest point so far in April 2020, 30\% fewer calls and 8\% longer mean turnaround times relative to January 2020 are observed. 
Although these results are not conclusive, it has to be noted that the COVID-19 pandemic had a significant impact on the number of ambulance calls and potentially also 
on turnaround times, thus limiting the representativeness of this dataset.
Potentially these effects can in part be explained by yearly seasonality.
However, as the dataset only contains the first half of the year 2020, it was not possible to gain further insights into this topic here. 


 \begin{figure}[h!]
    \includegraphics[width=15cm]{fig/analysis_detailed/by_month.png}
    \caption{New COVID-19 cases in Germany as well as observed ambulance calls and turnaround times relative to January 2020 \parencite{RKI}}
    \label{fig:covid}
\end{figure}
\FloatBarrier