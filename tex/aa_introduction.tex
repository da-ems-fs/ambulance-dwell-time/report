\section{Introduction}
A significant part of the time an ambulance is assigned to an incident is spent at the hospital, offloading the patient and afterwards getting ready for 
the next call.  The aim of this work is therefore to analyze this time interval and to understand the factors that influence it. 

\subsection{Motivation}
In the German emergency services, the duration from dispatch to vehicle on scene is an important quality criterion and a key performance indicator. 
Dispatchers generally aim to send the nearest available unit to the incident, a dispatch strategy that is mandated by law in some states.
Additionally, the number and geolocation of available units is closely monitored in order to be able to react to an increase in demand in a timely manner. 
If the dispatcher identifies a potential lack of resources in a certain area, they may call in on-call crews to staff reserve vehicles. 
Alternatively, the dispatcher may initiate a cover-move by preemptively dispatching available units from less busy areas to high-risk areas. 
These decisions generally only take into account how many of the units are unavailable at the moment, and not when they will statistically become available again. 
\parencite{paul_wehry_disposition_2018}

Not knowing when a unit will be available again may lead to some inefficiencies or potentially even increase response times.
This is because in some cases, a spike in demand may lead to a shortage in available ambulances, which in turn would lead dispatchers to react by calling in
additional personnel to staff backup units even though multiple units have nearly completed their calls and are bound to become available within the next few minutes. 
In other cases, units from further away may be dispatched to an incident or even just to cover a high-risk area even though there is a unit nearby that 
is just wrapping up their last call and that will likely become available very soon. 

The duration of an incident is always difficult to predict accurately, but some aspects are more difficult to predict than others. 
Driving times for example can be approximated with routing algorithms, but the factors influencing the time spent on scene are a lot more complex. 

With regard to the ambulance service, the last time interval of the call is usually the time spent at the hospital, offloading and transfering the
patient, finishing up the documentation and getting ready for the next call. 
It is the author's hope that this time interval can be predicted with some accuracy, which may contribute to more well informed dispatching decisions in the future. 



\subsection{Aim of This Research}
The aim of this research is therefore to better understand the time interval that ambulance crews spend at the hospital and to predict it with reasonable accuracy.
To support this research, a large dataset of ambulance calls was obtained and will be analyzed with the intent of quantifying the factors influencing the dwell times of
ambulances offloading patient at a hospital. 

The author recognizes that regardless of the accuracy of these predictions, dispatchers cannot rely on an ambulance \textit{probably} becoming available 
very soon and will therefore always have to confirm a unit's availability prior to dispatch. 
Consequently, the author does not intend to create a decision-making system but merely to lay the foundations for a decision-support system. 
\newline

\begin{tcolorbox}[width=14cm]

\underline{Research Questions:} 
\begin{itemize}
    \item How long does it take for an ambulance to become available again
    after arriving at the hospital to offload a patient?
    \item How much does this duration vary?
    \item Which are the main factors influencing this duration?
    \item Which categories should be differentiated between to minimize the variance within groups?
\end{itemize}
    
\end{tcolorbox}