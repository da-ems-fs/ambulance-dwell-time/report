\subsection{Distribution and Choice of Measures}
\begin{figure}[h]
    \centering
    \begin{adjustbox}{clip,trim=0cm 0.6cm 0cm 0.8cm}
    \scalebox{0.6}{\input{fig/analysis_detailed/histogram.pgf}}
    \end{adjustbox}
    \caption{Distribution of observed turnaround times}
    \label{fig:distr_histo}
\end{figure}

The distribution of observed turnaround times is skewed to the right, as is evident both visually in the histogram (Figure \ref{fig:distr_histo})
and more formally by the position of the mean to the left of the median. 
Since the distribution has a noticeable skew, quartiles and boxplots may not give an accurate picture of the distribution. 
For unimodal distributions, boxplots give a good idea of dispersion and skew. 
They were deemed not to be a good fit here because of the multimodality of the distribution in some groups. 
An additional reason was that the whiskers extend to cover the maximum, which distorts the perception of the tail of 
the distribution due to infrequent and highly random observations. 
Alternatively, the whiskers exclude outliers, which even after outlier removal are frequent, further complicating interpretation. 

The 10th and 90th percentile, which are typically thought of as containing the \textit{middle} 80\% of observations, 
span a large interval, because the 10th percentile \textit{cuts} into the peak and the 90th percentile is \textit{somewhere along} the tail. 

The interval between the 5th and 85th percentile for example would be much shorter while also containing 80\% of observations. 
In more general terms, the \textit{middle} 80\% of observations seems like a useful and intuitive measure that could help dispatchers in their decisionmaking. 
Therefore, it poses the question of how to reliably find the shortest interval containing, for example, 80\% of observations. 

In Bayesian statistics, when describing the posterior distribution, this measure is called the highest density interval \parencite{kruschke_doing_2015}. 
For symmetrical distributions, this is equivalent to the interpercentile range (Figure \ref{fig:HDI_normal}). 
In skewed distributions however, the highest density interval describes the shortest interval containing the 
respective amount of observations (Figure \ref{fig:HDI_skew}). 
For multimodal distributions, the highest density interval may be interrupted, producing multiple disjoint intervals (Figure \ref{fig:HDI_bimodal}). 

Borrowing this concept and translating it into the world of discrete data, the author adapted it as a \textit{continuous} highest density interval. 
This is consequently defined as the shortest continuous interval that contains a given percentage of observations (Figure \ref{fig:HDI_bimodal_continuous}). 
It is then implemented in a custom python function. 

The rationale behind this seemingly odd choice is that the author feels it is an intuitive, easy to understand representation of the likely duration
of a random event, even with a skewed, multimodal distribution. As a result, the dispatcher can be informed that, under the given conditions, 80\% of ambulances become available in the next 
x to y minutes. 

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/cHDI/norm.png}
        \caption{Interpercentile range}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/cHDI/norm_cHDI.png}
        \caption{Highest density interval}
    \end{subfigure}
    \caption{Interpercentile range and highest density interval of a normal distribution}
    \label{fig:HDI_normal}  
\end{figure*}

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/cHDI/skew_right.png}
        \caption{Interpercentile range}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/cHDI/skew_right_cHDI.png}
        \caption{Highest density interval}
    \end{subfigure}
    \caption{Interpercentile range and highest density interval of a skewed normal distribution}
    \label{fig:HDI_skew}
\end{figure*}

\begin{figure*}[h!]
    \centering
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/cHDI/data_norm_bimodal_HDI.png}
        \caption{Highest density interval}  
        \label{fig:HDI_bimodal}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{8cm}
        \centering
        \includegraphics[width=8cm]{fig/cHDI/data_norm_bimodal_cHDI.png}
        \caption{Continuous highest density interval}
    \end{subfigure}
    \caption{Conventional and continuous highest density interval of a bimodal distribution}
    \label{fig:HDI_bimodal_continuous}
\end{figure*}

