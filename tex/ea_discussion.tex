\section{Discussion}

\subsection{Limitations of Data Collection and Recording}
The technical process of FMS status logging, either via radio or via rescuetrack, is relatively error-free. 
With the rescuetrack logging system used here, a loss of data is unlikely and can only feasibly happen when the unit is powered down completely before 
being able to send the status. The timestamp accuracy is definitely sufficient, as it is acquired from GPS satellites.
Even though technical errors seem unlikely, it would still be advisable to compare the dataset used here with the dataset generated 
by the incident control room using FMS status received via radio. 
Since the author's time spent working on this project was paid by Convexis GmbH, the company behind rescuetrack, there may be subconscious biases. 
Colleagues at Convexis GmbH did help with technical questions, however, no one attempted to influence the results of this work in any way. 
The author does provide the entire source code of this analysis as a public git repository and invites others to reproduce it. See section \ref{subs:tech} for details. 

A likely cause of invalid data seems to be human error, with crews sometimes forgetting to press the button signaling a 
status change and advancing or delaying the change either intentionally or unintentionally \parencite{paul_wehry_disposition_2018}. 
Therefore, the author suggests that this phenomenon is studied, both to find out the extent to which manually set status in dispatch system logs 
are valid and to understand what the reasons behind possible deviations are. 
It may be possible to use GPS data or vehicle sensors available via CAN bus such as door open/closed, handbrake on/off and gas pedal position along
with the status log to find patterns in how status change and actual arrival and departure times differ. 
This should be done in a blame-free way with the only goal being to better understand and support crews. 
The negative effects of the pressure put on ambulance crews by the increased focus on performance metrics are described by L. Price \parencite*{Price127}.

Overall, this analysis would have benefited from a larger, more diverse study area and a larger period of available data. 
When using data gathered over decades, possible changes in recording technology and their impact on data validity should be kept in mind. 
The COVID-19 pandemic also impacted the data used here, but the effects can not be quantified since data of the previous years was not available for comparison. 

The destination names were manually matched for hospital names to exclude residential adresses, but some valid data may have been lost in the process. 
Also, hospitals were not classified into broader categories, for example size, specialties, or walking routes from ambulance parking to the emergency room. 

\subsection{Limitations of Data Analysis}
The method chosen for analysis was sufficient for identifying some general trends in the data, but a more robust method is definitely needed. 
To better quantify the differences between groups, statistical testing or a multivariate analysis of variance seem to be adequate tools. 
Additionally, it may be helpful to reduce the dimensionality of the dataset using princial component analysis. 
In order to better deal with the seasonality in the time series data present here, time series analysis tools such as 
autoregressive integrated moving average [ARIMA] could be benefitial here. 
When attempting to make accurate predictions of turnaround time, rather than using the four-dimensional table spliting 
turnaround times by hospital, time of day, and incident code described here,  machine learning algorithms would not only potentially allow 
for better predictions but for a continuous adaptation of the prediction system as new data is gathered. 

Outlier detection should definitely be improved, preferably understanding how invalid data even gets into the dataset in the first place. 
However, it is the author's belief that the \textit{expert rule} of \textit{over 10 seconds and under 3 hours} is a good starting point and 
that this methods yields results of sufficient validity. 
In a similar manner, NaN values should be investigated more thoroughly as valid data may be lost due to technical errors. 

An oversight made during this analysis was that \textit{time of day} was determined by the start of the call instead of the arrival at the hospital. 
Also, only urgent calls were analysed, therefore only allowing insights into emergency ambulance calls and not patient transport. 

\subsection{Suggestions}
Aside from needing to use better, more advanced and more suitable statistical methods, a main issue was data availability. 
Therefore, this study should be repeated using data from a longer timeframe, a larger and more diverse geograpical area.

Qualitative understanding of the different sub-intervals of turnaround time and the reasons for delay should also be improved in order to be available to 
improve the accuracy of predicted turnaround times.

Crew health and wellfare need to be kept in mind, they need to \textit{treat the patient, not the clock} \parencite{Price127}.
