Note that this codes only generates some explanatory figures. 
The plots and diagrams are manually copied from the main code repository. 
Install using `pip3 install -e fig/python` from repo root or `pip3 install -e .` from here.