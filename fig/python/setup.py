"""
to manually install the module in ./fig/python/src run
pip3 install -e fig/python
from project root
"""

from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    author='davus.g@gmail.com',
    license='proprietary',
)