import pandas as pd
import matplotlib.pyplot as plt

fig, ax = plt.subplots()

ax.arrow(.1, .1,.9,.1, label="base")

fig.patch.set_visible(False)
ax.axis('off')

plt.show()
