import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import matplotlib.dates as mdates

def plot(data_dir, fig_dir):
    covid = pd.read_excel(data_dir+"/Nowcasting_Zahlen.xlsx", sheet_name=1, parse_dates=True)
    covid = covid.set_index("Datum des Erkrankungsbeginns")
    turnaround_times = pd.read_csv(data_dir+"/results_turnaround_times_by_month.csv")
    turnaround_times["Month"] = pd.to_datetime("2020-"+turnaround_times["Month"].astype(str)+"-01")
    turnaround_times = turnaround_times.set_index("Month")
    turnaround_times_rel = compute_percentage_of_baseline(turnaround_times)

    fig, ax1 = plt.subplots()
    ax1.axhline(1, linewidth=0.5, color="grey")
    ax1 = plot_barplot_of(turnaround_times_rel, ax1)

    ax2 = ax1.twinx()
    ax2 = plot_lineplot_of(covid, ax2)

    fig.legend(loc='upper center', bbox_to_anchor=(0.5, 1), ncol=4)
    ax1.yaxis.tick_left()
    ax1.set_ylim(0,1.2)
    ax1.yaxis.set_major_formatter(PercentFormatter(xmax=1))
    ax2.set_ylim(0.6000)
    ax2.set_yticklabels([0,1000,"",3000,"",5000,""])
    ax2.yaxis.tick_right()
    ax2.set_ylabel("New COVID-19 cases per day", labelpad=10)
    ax1.set_ylabel("Calls and turnaround times relative to Jan 2020")

    ax1.xaxis.set_major_locator(mdates.MonthLocator())
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%b %Y'))
    ax1.set_xlim(pd.to_datetime("2019-12-29"),pd.to_datetime("2020-06-04"))
    fig.autofmt_xdate()
    fig.subplots_adjust(right=0.87, bottom=0.15)

    plt.savefig(fig_dir+"/analysis_detailed/by_month.png", bbox_inches=0, dpi=800)

def plot_lineplot_of(data, ax):
    ax.plot(data["Punktschätzer der Anzahl Neuerkrankungen (ohne Glättung)"], color="black", label="COVID-19 cases")
    return ax

def plot_barplot_of(data, ax):
    ax.plot(data.index, data["count"], "o", ls=":", label="number of calls")
    #ax.plot(data.index, data["median"], "o", ls=":", label="median turnaround time")
    ax.plot(data.index, data["mean"],"o", ls=":", label="mean turnaround time")
    return ax


def compute_percentage_of_baseline(data):
    data = data.copy()
    for column in data.columns:
        data[column] = data[column]/data[column].iloc[0]
    return data