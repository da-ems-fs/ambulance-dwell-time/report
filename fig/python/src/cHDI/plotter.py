import matplotlib.pyplot as plt
import numpy as np

def plot_histogram(data, fig_dir, fig_name, percentiles=[]):
    plt.hist(data, bins=50)
    mark_percentiles(data, percentiles, ylims=plt.ylim())
    fillbetween(data, percentiles)
    plt.savefig(f"{fig_dir}/cHDI/{fig_name}.png")
    plt.close()

def mark_percentiles(data, percentiles, ylims):
    for percentile in percentiles: 
        x_position=np.percentile(data, percentile)
        plt.axvline(x_position, color='r', linestyle='dashed', linewidth=1.5, ymax=0.9)
        plt.text(x_position, ylims[1]*0.9, f"p{round(percentile,2)}", ha="center")

def fillbetween(data, percentiles):
    x_positions=np.percentile(data, percentiles)
    plt.axvspan(x_positions[0], x_positions[1], color="grey", alpha=0.5)