import numpy as np

def cHDI(data, percentage):
    percentage_range = percentage/100
    data_length = len(data)
    data_range = int(percentage_range*data_length)
    data_sorted=sorted(data)
    beginning_of_smallest_range = find_beginning_of_smallest_range(data_sorted, data_range, data_length)
    end_of_smallest_range = find_end_of_smallest_range(beginning_of_smallest_range, data_range)
    percentiles = find_corresponding_percentiles(beginning_of_smallest_range, end_of_smallest_range, data_length)
    percentile_values = find_percentile_values(data_sorted, beginning_of_smallest_range, end_of_smallest_range)
    return percentiles, percentile_values, data_range

def find_beginning_of_smallest_range(data_sorted, data_range, data_length):
    possible_startpoints = range(data_length-data_range+1)
    result = []
    for startpoint in possible_startpoints:
        low=startpoint
        high=startpoint+data_range-1
        value_range=data_sorted[high]-data_sorted[low]
        result.append(value_range)
    beginning_of_smallest_range = np.argmin(result)
    return beginning_of_smallest_range

def find_end_of_smallest_range(beginning_of_smallest_range, data_range):
    return beginning_of_smallest_range + data_range

def find_corresponding_percentiles(beginning_of_smallest_range, end_of_smallest_range, data_length):
    return [beginning_of_smallest_range/data_length*100, (end_of_smallest_range)/data_length*100]

def find_percentile_values(data_sorted, beginning_of_smallest_range, end_of_smallest_range):
    return [data_sorted[beginning_of_smallest_range], data_sorted[end_of_smallest_range]]