from scipy.stats import skewnorm
import numpy as np

def generate_datasets():
    norm = skewnorm.rvs(0, size=100000, random_state=15)
    norm_sub0 = norm[norm<0]
    data_norm = np.concatenate((norm_sub0, np.absolute(norm_sub0)))
    data_skew_right = skewnorm.rvs(15, size=100000, random_state=15)
    data_norm_offset = skewnorm.rvs(0, size=50000, random_state=15, loc=5)
    data_norm_bimodal = np.concatenate((data_norm, data_norm_offset))
    simple = [1,1,1,1,1,1,1,10,11,1]
    return data_norm, data_skew_right, data_norm_bimodal, simple