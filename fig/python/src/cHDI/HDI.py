import matplotlib.pyplot as plt
import numpy as np

def HDI_hist(data, fig_dir, fig_name, percentage):
    x, bins, pt = plt.hist(data, bins=50)
    n = approx_n(data, percentage, x)
    indices_of_top_n_bins = np.argsort(x)[-n:]
    top_n_bins_lower_limits=bins[indices_of_top_n_bins]
    bin_width=(np.max(data)-np.min(data))/len(bins)
    limits_of_HDI=find_limits_of_HDI(top_n_bins_lower_limits, bin_width)
    plot_limits_of_HDI(data, limits_of_HDI, plt.ylim())
    plt.bar(top_n_bins_lower_limits, np.max(x), width=bin_width, align="edge", color="grey", alpha=0.5)
    plt.savefig(f"{fig_dir}/cHDI/{fig_name}.png")
    plt.close()

def find_limits_of_HDI(top_n_bins_lower_limits, bin_width):
    top_n_bins_lower_limits_sorted=sorted(top_n_bins_lower_limits)
    gaps=np.where(np.diff(top_n_bins_lower_limits_sorted)>bin_width*1.1)[0]
    minim=np.amin(top_n_bins_lower_limits_sorted)
    maxim=np.amax(top_n_bins_lower_limits_sorted)+bin_width
    limits=[minim, maxim]
    for gap in gaps:
        lower=top_n_bins_lower_limits_sorted[gap]+bin_width
        upper=top_n_bins_lower_limits_sorted[gap+1]
        limits.append(lower)
        limits.append(upper)
    return limits

def plot_limits_of_HDI(data, limits_of_HDI, ylims):
    x_positions=limits_of_HDI
    for x_position in x_positions:
        plt.axvline(x_position, color='r', linestyle='dashed', linewidth=1.5, ymax=0.9)
        percentile=find_percentile_of_val(data, x_position)
        plt.text(x_position, ylims[1]*0.9, f"p{round(percentile*100,2)}", ha="center")

def find_percentile_of_val(data, val):
    percentile=find_index_of_nearest(sorted(data), val)/len(data)
    return percentile
    
def find_index_of_nearest(array, val):
    index=(np.absolute(array-val)).argmin()
    return index

def approx_n(data, percentage, x):
    target = len(data)*percentage/100
    cumsum = np.cumsum(sorted(x, reverse=True))
    n = find_index_of_nearest(cumsum, target)
    return n