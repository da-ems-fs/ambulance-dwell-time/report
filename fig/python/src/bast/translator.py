import pandas as pd

cols1 = [
    "year from",
    "year to",
    "traffic accident \n [Verkehrsunfall]",
    "work accident \n [Arbeitsunfall]",
    "other accident \n [Sonstiger Unfall]", 
    "internal medical emergency \n [Internistischer Notfall]", 
    "other emergency \n [Sonstiger Notfall]",
    "patient transport \n [Krankentransport]"
]


cols2 = [
    'year from',
    'year to',
    'emergency incident w. doctor \n [Notarzteinsatz]',
    'emergency incident \n [Notfalleinsatz]',
    'urgent patient transport \n [Dringlicher Krankentransport]',
    'non-urgent patient transport \n [Disponibler Krankentransport]']

cols3 = [
    'year from',
    'year to',
    'with lights and siren',
    'without lights and siren']


def translate(dfs):
    for df, cols in zip(dfs.values(),[cols1,cols2,cols3]):
        print(cols)
        df.columns = cols
    return dfs