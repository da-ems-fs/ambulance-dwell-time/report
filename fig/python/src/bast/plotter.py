import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors
from src.bast.translator import translate

def plot(data_dir, fig_dir):
    dfs = pd.read_excel(data_dir+"/BASt_turnaround_times.xls", sheet_name=None)
    dfs = translate(dfs)
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list('name', ['lightgrey', 'black'])
    for name, df in dfs.items():
        df["year"] = df["year from"].astype(str) + "/" + df["year to"].astype(str).str.slice(start=-2)
        df.drop(columns=["year from", "year to"], inplace=True)
        df = df.pivot_table(columns="year")
        df.plot.barh(legend=False, colormap=cmap, xlim=(0,23))
        plt.tight_layout()
        plt.gcf().legend(loc='upper center', bbox_to_anchor=(0.5, 1.04), ncol=4)
        #plt.gcf().subplots_adjust(left=0.4)
        plt.savefig(fig_dir+f"/bast/{name}.png", dpi=800, bbox_inches="tight")