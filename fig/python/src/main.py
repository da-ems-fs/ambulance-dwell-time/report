import src.cHDI.distributions
import src.cHDI.cHDI_example
import src.cHDI.plotter
import src.cHDI.HDI
import src.months.plotter
import src.bast.plotter

fig_dir = "fig"
data_dir = "public_data_for_comparison"

def cHDI_plots():
    data_norm, data_skew_right, data_norm_bimodal, data_norm_simple = src.cHDI.distributions.generate_datasets()
    src.cHDI.plotter.plot_histogram(data_norm, fig_dir, fig_name="norm", percentiles=[25,75])
    src.cHDI.plotter.plot_histogram(data_skew_right, fig_dir, fig_name="skew_right", percentiles=[25,75])
    percentiles_norm, percentile_values_norm, data_range_norm = src.cHDI.cHDI_example.cHDI(data_norm, percentage=50)
    src.cHDI.plotter.plot_histogram(data_norm, fig_dir, fig_name="norm_cHDI", percentiles=percentiles_norm)
    percentiles_skew, percentile_values_skew, data_range_skew = src.cHDI.cHDI_example.cHDI(data_skew_right, percentage=50)
    src.cHDI.plotter.plot_histogram(data_skew_right, fig_dir, fig_name="skew_right_cHDI", percentiles=percentiles_skew)
    
    src.cHDI.plotter.plot_histogram(data_norm_bimodal, fig_dir, fig_name="data_norm_bimodal", percentiles=[10,90])
    percentiles_norm_bimodal, percentile_values_bimodal, data_range_bimodal = src.cHDI.cHDI_example.cHDI(data_norm_bimodal, percentage=80)
    src.cHDI.plotter.plot_histogram(data_norm_bimodal, fig_dir, fig_name="data_norm_bimodal_cHDI", percentiles=percentiles_norm_bimodal)

    src.cHDI.plotter.plot_histogram(data_norm_simple, fig_dir, fig_name="data_norm_simple", percentiles=[25,75])
    percentiles_norm_simple, percentile_values_simple, data_range_simple = src.cHDI.cHDI_example.cHDI(data_norm_simple, percentage=50)
    src.cHDI.plotter.plot_histogram(data_norm_simple, fig_dir, fig_name="data_norm_simple_cHDI", percentiles=percentiles_norm_simple)
    #note that this does work as expected, in plotting numpy interpolates the percentile position, which is what it should do. 


def HDI_plots():
    data_norm, data_skew_right, data_norm_bimodal, data_norm_simple = src.cHDI.distributions.generate_datasets()
    src.cHDI.HDI.HDI_hist(data_norm_bimodal, fig_dir, "data_norm_bimodal_HDI", 80)

def months_plots():
    src.months.plotter.plot(data_dir, fig_dir)

def bast_plot():
    src.bast.plotter.plot(data_dir, fig_dir)


cHDI_plots()
HDI_plots()

months_plots()

bast_plot()